const path = require('path')
const HtmlPlugin = require('html-webpack-plugin')
const ExtractTextPlugin = require('extract-text-webpack-plugin')

module.exports = {
	entry: __dirname + '/src/index.js',
	output: {
		path: path.join(__dirname, 'build'),
		filename: 'bundle.js',
		publicPath: '/'
	},
	devServer: {
		open: true,
		overlay: true,
		historyApiFallback: true
	},
	resolve: {
		alias: {
			'@': path.join(__dirname, 'src'),
			'vue$': 'vue/dist/vue.esm.js'
		},
		extensions: ['.js', '.json', '.vue']
	},
	module: {
		rules: [
			{
				test: /\.vue$/,
				loader: 'vue-loader?extractCSS'
			},
			{
				test: /\.css$/,
				use: ExtractTextPlugin.extract({
					use: 'css-loader'
				})
			},
			{
				test: /\.sass$/,
				use: ExtractTextPlugin.extract({
					use: 'sass-loader!css-loader'
				})
			}
		]
	},
	plugins: [
		new ExtractTextPlugin('style.css'),
		new HtmlPlugin({
			template: path.join(__dirname, 'index.html'),
			filename: 'index.html'
		})
	]
}