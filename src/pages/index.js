import Home from '@/pages/Home'
import Signin from '@/pages/Signin'
import Signup from '@/pages/Signup'
import Profile from '@/pages/Profile'
import Todos from '@/pages/Todos'
import NotFound from '@/pages/NotFound'

export default {
	Home, Signin, Signup,
	Profile, Todos, NotFound
}