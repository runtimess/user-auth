const mysql = require('promise-mysql')
let conn = null



exports.get = () => conn

exports.connect = async options => {
	if (conn) return conn

	const connection = await mysql.createConnection(options)
	conn = connection
	return conn
}