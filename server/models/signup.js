const db = require('./')


exports.createUser = async (email, password) => {
	return await db.get().query('INSERT INTO users SET email = ?, password = ?', [email, password])
}